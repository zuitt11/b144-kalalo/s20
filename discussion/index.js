// JSON Objects
// JavaScript Object Notation
	// used for serializing different data types into bytes.
	// SERIALIZATION - process of converting data into series of bytes for easier transmission or transfer of information.
	// Byte - binary language (1 & 0) which is used to represent a character.

// JSON DATA FORMAT
	/*
	{
			'propertyA: "valueA"'
			'propertyB: "valueB"'
	}
	*/

// ********DIFFERENCE*******

// JS Object
/*
{
	city: "QC"
	province: "Metro Manila"
	country: "Philippines"
}
*/


// JSON

/*
{
	"city": "QC"
	"province": "Metro Manila"
	"country": "Philippines" 
}
*/

//JSON Arrays

/*
"cities": [
{
	"city": "QC"
	"province": "Metro Manila"
	"country": "Philippines" 
}
{
	"city": "QC"
	"province": "Metro Manila"
	"country": "Philippines" 
}
{
	"city": "QC"
	"province": "Metro Manila"
	"country": "Philippines" 
}
]
*/


//JSON METHODS
	//JSON Object contains methods for parsing and converting data into stringified JSON
	//Stringified JSON - JS Object converted into string to be used in other function of a Javascript application
	let batchesArr = [
	{
		batchName: 'Batch X'
	},
	{
		batchName: 'Batch Y'
	},
	]

	console.log(`Result from stringify method`);
	// strigify is used to convert J Objectss into JSON (string)
	console.log(JSON.stringify(batchesArr));

	let data = JSON.stringify({
		name:'John',
		age:31,
		address:{
			city: 'Manila',
			country: 'Philippines'
		}
	})
	console.log(data)


let userDetails = JSON.stringify({
	fname:prompt('Please input your first name.'),
	lname:prompt('Please input your last name.'),
	age:prompt('Please input your age.'), 
	address: {
		city: prompt('City'),
		country: prompt('Country'),
		zipCode: prompt('Zipcode')
	}
})

console.log(userDetails)


// Converting Stringified JSON into JS Objects
	// Information is commonly sent to application in stringified JSON and then converted back into objects; this happens both for sending information to a backend app and back to frontend app;
	//upon recieving data, JSON text can be converted to a JSON Object with parse

	let batchesJSON = `[
	{
		"batchName": "Batch X"
	},
	{
		"batchName": "Batch Y"
	}
	]`

	console.log('Results from parse method:')
	console.log(JSON.parse(batchesJSON))

	let stringifiedObject = `
		{
			"name": "Johny",
			"age": "31",
			"address": {
				"city": "Manila",
				"country": "Philippines"
			}
		}
	`
	console.log(JSON.parse(
		stringifiedObject))
// 
